# Phaser 3 - Groot the Savior
Groot, uma criatura com aparência de uma árvore Humanoide, vê o seu planeta ser dominado pelo seu inimigo, Phalanx, que em tempos o tivera quase matado durante uma batalha.
Decidido a reaver de novo o Planeta X, luta com toda a sua força contra as naves do inimigo que disparam sobre ele, usando a sua habilidade, os esporos de luz. 
Durante a sua batalha consegue ainda apanhar poeiras espaciais que ajudam a sua habilidade a tornar-se ainda mais poderosa. 
Ajuda o Groot a derrotar o Phalanx e a salvar o Planeta X!


## Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

## Run Project

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |

