import {
    Scene
} from "phaser";
import Sky from './assets/images/background.jpg';
import Groot from './assets/images/groot.png';

class PreloadScene extends Scene {
    constructor() {
        super('preload');
    }

    //Função para carregar a scene
    preload() {
        this.load.image('sky', Sky);
        this.load.image('groot', Groot);
    }

    //Função apra criar a scene
    create() {
        //Adiciona o background
        const sky = this.add.image(0, 0, 'sky');
        sky.setOrigin(0, 0);
        this.add.image(100, 455, 'groot').setScale(.5);

        this.title = this.add.text(280, 380, 'Groot the Savior', {
            fontSize: '50px',
            fill: '#FFD700'
        });

        this.title.setOrigin(0.5);
        this.title.visible = true;

        this.subText = this.add.text(280, 450, 'Press to start', {
            fontSize: '34px',
            fill: '#fff'
        });

        this.subText.setOrigin(0.5);
        this.subText.visible = true;

        this.input.on('pointerdown', () => {
            this.scene.start('game');
        });
    }
}

export default PreloadScene