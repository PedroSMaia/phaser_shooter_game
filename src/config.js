import Phaser from "phaser";
import GameScene from "./GameScene";
import PreloadScene from "./PreloadScene";

const config = {
    type: Phaser.AUTO,
    parent: "phaser-example",
    width: 560,
    height: 700,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                x: 0,
                y: 0
            },
            debug: false
        }
    },
    scene: [PreloadScene, GameScene]
};

export {
    config
}