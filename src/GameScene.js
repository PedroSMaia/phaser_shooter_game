import {
    Scene
} from 'phaser';
import Sky from './assets/images/background.jpg';
import Player from './assets/images/player.png';
import Enemy from './assets/images/enemy0.png';
import BulletPlayer from './assets/images/bullet_player.png';
import BulletEnemy from './assets/images/bullet_enemy.png';
import ScoreIcon from './assets/images/powerUp.png';
import particleConfig from './particlesConfig';
import Explosion from './assets/images/explosion.png';
import Asteroid from './assets/images/asteroid.png';


var lastBulletFired = 0;
var lastBulletSpecial = 0;


class GameScene extends Scene {
    //Construtor do jogo
    constructor() {
        super('game');
        //inicia o score a 0
        this.score = 0;
        //inicia o estado do gameOver como false
        this.gameOver = false;
        //Numero de vidas do jogadore
        this.lives = 3;
        //Verifica se o player está vivo
        this.isAlive = true;
        //Variável auxiliar de escore
        this.auxScore = 0;
        //Estado do powerUp
        this.powerUpStatus = false;
        //PowerUp timer
        this.powerUpTime = 0;
    }

    //Função para dar load da scene
    preload() {
        //Carrega as imagens
        this.load.image('sky', Sky);
        this.load.image('player', Player);
        this.load.image("bulletPlayer", BulletPlayer);
        this.load.image("bulletEnemy", BulletEnemy);
        this.load.image("scoreIcon", ScoreIcon);
        this.load.image("asteroid", Asteroid);
        this.load.image("powerUp", ScoreIcon);


        //Carrega os Sprites
        this.load.spritesheet("enemy", Enemy, {
            frameWidth: 16,
            frameHeight: 16
        });

        this.load.spritesheet('explosion',
            Explosion, {
                frameWidth: 22,
                frameHeight: 22
            }
        );

        //Carrega o audio
        this.load.audio('audio_back', 'src/assets/audio/bgm.mp3');
        this.load.audio('soundPlayerDead', 'src/assets/audio/stomp.wav');
        this.load.audio('soundExplosion', 'src/assets/audio/explosion.wav');
        
    }

    //Função para criar a scene
    create(time) {
        //Define o score como 0
        this.score = 0;
        //Define o AuxScore como 0
        this.auxScore = 0;
        //Define as vidas do player como 3
        this.lives = 3;
        //Define o estado do player como true
        this.isAlive = true;
        //Power Up timer
        this.powerUpTime = 0;

        //Adiciona o background
        const sky = this.add.image(0, 0, 'sky');
        sky.setOrigin(0, 0);

        //Adiciona o som e da play do mesmo em loop
        this.soundBack = this.sound.add('audio_back');
        this.soundBack.play();
        this.soundBack.setLoop(true);

        //Criação dos grupos
        this.bulletsPlayer = this.physics.add.group();
        this.bulletsEnemy = this.physics.add.group();
        this.enemies = this.physics.add.group();
        this.asteroids = this.physics.add.group();
        this.powerUps = this.physics.add.group();

        //Criação das animações
        this.anims.create({
            key: "enemy",
            frames: this.anims.generateFrameNumbers("enemy"),
            frameRate: 20,
            repeat: -1
        });

        //Chamadas das funções
        this.createPlayer();
        this.createCursor();
        this.createScore();
        this.createLivesPlayer();
        this.createParticles();
        this.createAsteroid();
        this.createPowerUp();
        
        //Texto de Game Over
        this.gameOverText = this.add.text(280, 330, 'Game Over', {
            fontSize: '50px',
            fill: '#ffffff'
        });
        this.gameOverText.depth = 100;
        this.gameOverText.setDepth(100);

        this.gameOverText.setOrigin(0.5);
        this.gameOverText.visible = false;

        //Texto de Final Score
        this.finalScoreText = this.add.text(280, 330, 'Score: ' + this.score, {
            fontSize: '44px',
            fill: '#FFD700'
        });
        this.finalScoreText.depth = 100;
        this.finalScoreText.setDepth(100);

        this.finalScoreText.setOrigin(0.5);
        this.finalScoreText.visible = false;

        //Time event para gerar os inimigos
        this.time.addEvent({
            delay: 4000,
            callback: function () {
                //Random para determinar o número de inimigos
                const numEnemy = Phaser.Math.Between(1, 7);
                for (let i = 0; i < numEnemy; i++) {
                    //Determinar o valor de x consoante a posição do player
                    const x = Phaser.Math.Between(10, 550);
                    //Determinar o valor de x e y consoante a posição do player
                    const y = Phaser.Math.Between(0, 20);
                    const vel = Phaser.Math.Between(50, 130);
                    //Cria os inimigos, define a sua velocidade e faz disparar as balas
                    this.enemy = this.enemies.create(x, y, 'enemy');
                    this.enemy.setVelocityY(vel);
                    this.addbulletEnemy(this.enemy.body.x, this.enemy.body.y, this.enemy.body.velocity.y);
                }
            },
            callbackScope: this,
            loop: true
        });

        //Criação dos overlaps entre os diversos elementos
        this.physics.add.overlap(this.bulletsPlayer, this.enemies, this.bulletPlayerHitEnemies, null, this);
        this.physics.add.overlap(this.player, this.bulletsEnemy, this.bulletEnemyHitPlayer, null, this);
        this.physics.add.overlap(this.enemies, this.player, this.enemyHitPlayer, null, this);
        this.physics.add.overlap(this.asteroids, this.enemies, this.asteroidHitEnemy, null, this);
        this.physics.add.overlap(this.asteroids, this.player, this.asteroidHitPlayer, null, this);
        this.physics.add.overlap(this.bulletsPlayer, this.asteroids, this.bulletPlayerHitAsteroids, null, this);
        this.physics.add.overlap(this.bulletsEnemy, this.asteroids, this.bulletEnemyHitAsteroids, null, this);
        this.physics.add.overlap(this.bulletsEnemy, this.bulletsPlayer, this.bulletEnemyHitbulletsPlayer, null, this);
        this.physics.add.overlap(this.player, this.powerUps, this.activatePowerUp, null, this);
    }

    //Função para criar as particulas das explosoes
    createParticles() {
        //Adiciona as particuals as moedas
        this.particles = this.add.particles('explosion');
        //Cria o emitter com as respectivas configurações das particulas
        this.emitter = this.particles.createEmitter(particleConfig);
    }

    //Função para criar o Player
    createPlayer() {
        //Cria um novo objeto com as propriedades fisicas
        this.player = this.physics.add.image(280, 630, 'player');
        //Define a área de contacto do player
        this.player.setCircle(13, 0, 0);
        //Define que o player colide com o mundo "Area de Jogo"
        this.player.setCollideWorldBounds(true);
    }

    //Função para criar os aeteroides
    createAsteroid() {
        this.time.addEvent({
            delay: 2800,
            callback: function () {
                //Random para determinar o lado que o asteroid vai aparecer
                const side = Phaser.Math.Between(0, 3);

                if (side == 0) {
                    //Random para determinar a posição Y que o player vai ser gerado
                    const sideY = Phaser.Math.Between(20, 680);
                    //Cria um novo objeto com as propriedades fisicas
                    this.asteroid = this.asteroids.create(-20, sideY, 'asteroid');
                    //Define a área de contacto do asteroid, velocidade e permite gravidade
                    this.asteroid.setCircle(13, 0, 0);
                    this.asteroid.setVelocityX(Phaser.Math.Between(100, 150));
                    this.asteroid.body.setAllowGravity(false);
                } else if (side == 1) {
                    //Random para determinar a posição Y que o player vai ser gerado
                    const sideX = Phaser.Math.Between(20, 560);
                    //Cria um novo objeto com as propriedades fisicas
                    this.asteroid = this.asteroids.create(sideX, -20, 'asteroid');
                    //Define a área de contacto do asteroid, velocidade e permite gravidade
                    this.asteroid.setCircle(13, 0, 0);
                    this.asteroid.setVelocityY(Phaser.Math.Between(100, 150));
                    this.asteroid.body.setAllowGravity(false);

                } else if (side == 2) {
                    //Random para determinar a posição Y que o player vai ser gerado
                    const sideY = Phaser.Math.Between(20, 680);
                    //Cria um novo objeto com as propriedades fisicas
                    this.asteroid = this.asteroids.create(580, sideY, 'asteroid');
                    //Define a área de contacto do asteroid, velocidade e permite gravidade
                    this.asteroid.setCircle(13, 0, 0);
                    this.asteroid.setVelocityX(Phaser.Math.Between(-100, -150));
                    this.asteroid.body.setAllowGravity(false);
                }
            },
            callbackScope: this,
            loop: true
        });
    }

    //Função para criar o powerUp
    createPowerUp() {
        this.time.addEvent({
            delay: 35000,
            callback: function () {
                //Random para determinar a posição X que o player vai ser gerado
                const sideX = Phaser.Math.Between(20, 560);
                //Cria um novo objeto com as propriedades fisicas
                this.powerUp = this.powerUps.create(sideX, -20, 'powerUp');
                //Define a área de contacto do powerUP, velocidade e permite gravidade
                this.powerUp.setCircle(13, 0, 0);
                this.powerUp.setVelocityY(Phaser.Math.Between(100, 150));
                this.powerUp.body.setAllowGravity(false);
            },
            callbackScope: this,
            loop: true
        });
    }
    
    //Função para criar o score board
    createScore() {
        //Cria um scoreBoard
        this.scoreBoard = this.physics.add.staticGroup();
        //Adiciona o Icon do score
        this.scoreBoard.create(30, 33, 'scoreIcon');
        //Define o score inicial como 0
        this.scoreText = this.add.text(50, 16, '0', {
            fontSize: '32px',
            fill: '#fff'
        });
    }

    //Criação das vidas do player
    createLivesPlayer() {
        //Cria um scoreBoard
        this.livesBoard = this.physics.add.staticGroup();
        //Adiciona o Icon das vidas
        this.livesBoard.create(510, 33, 'player');
        //Define o score inicial como 0
        this.livesText = this.add.text(530, 16, '3', {
            fontSize: '32px',
            fill: '#fff'
        });
    }

    //Função para criar as teclas
    createCursor() {
        this.cursors = this.input.keyboard.createCursorKeys();
        this.keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        this.keySpecial = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.C);
    }

    //Função para criar as balas do player
    createPlayerBullet(x, y, vy) {
        //Cria a bala
        this.bulletPlayer = this.bulletsPlayer.create(x, y, 'bulletPlayer');
        //Define a velocidade de Y
        this.bulletPlayer.body.velocity.y = vy;
        //Permite a gravidade
        this.bulletPlayer.body.setAllowGravity(false);
    }

    //Adiciona a bala ao player
    addbullet(x, y, vy) {
        this.createPlayerBullet(x + 15, y, -400);
    }

    //Função para criar a bola do inimigo
    createEnemyBullet(x, y, vy) {
        //Cria a bala
        this.bulletEnemy = this.bulletsEnemy.create(x, y, 'bulletEnemy');
        //Define a velocidade de Y
        this.bulletEnemy.body.velocity.y = vy;
        //Permite a gravidade
        this.bulletEnemy.body.setAllowGravity(false);
    }

    //Adiciona a bullet para o inimigo
    addbulletEnemy(x, y, vy) {
        this.createEnemyBullet(x + 10, y+20, 400);
    }

    //Função para criar o ataque especial
    createSpecialPlayerBullet(x, y, vx, vy) {
        //Cria a bala
        this.bulletPlayer = this.bulletsPlayer.create(x, y, 'bulletPlayer');
        //Define a velocidade de X e Y
        this.bulletPlayer.body.velocity.x = vx;
        this.bulletPlayer.body.velocity.y = vy;
    }

    //Adiciona o ataque especial ao player
    addSpecialBullet(x, y, vx, vy) {
        this.createPlayerBullet(x, y, -400, -400);
        this.createPlayerBullet(x + 15, y, -400, 400);
        this.createPlayerBullet(x + 30, y, -400, -400);
        
    }

    //Colisao entre a bala do player e o inimigo
    bulletPlayerHitEnemies(bulletsPlayer, enemies){
        //Cria a particulas quando existe a colisao
        this.particles.emitParticleAt(enemies.x, enemies.y, 50);
        //Lança o som
        let soundExplosion = this.sound.add('soundExplosion');
        soundExplosion.setVolume(1);
        soundExplosion.play();
        //Destroi a bala e o inimigo
        bulletsPlayer.destroy();
        enemies.destroy();
        //Aumenta o score  e o auxScore +1
        this.score += 1;
        this.auxScore += 1;
        //Define o novo score
        this.scoreText.setText(this.score);
        //Verifica se o auxScore == 30
        if (this.auxScore == 30) {
            //Aumenta a vida
            this.lives += 1;
            //Define a vida
            this.livesText.setText(this.lives);
            //Define o auxScore como 0
            this.auxScore = 0;
        }
    }

    //Define a colisão entre asteroide e inimigo
    asteroidHitEnemy(asteroids, enemies){
        //Cria a particulas quando existe a colisao
        this.particles.emitParticleAt(enemies.x, enemies.y, 50);
        //Lança o som
        let soundExplosion = this.sound.add('soundExplosion');
        soundExplosion.setVolume(1);
        soundExplosion.play();
        //Destroi a asteroide e o inimigo
        asteroids.destroy();
        enemies.destroy();
    }

    //Define a comisão entre a bala do inimigo e a bala do player
    bulletEnemyHitbulletsPlayer(bulletPlayer, bulletEnemy){
        //Cria a particulas quando existe a colisao
        this.particles.emitParticleAt(bulletEnemy.x, bulletEnemy.y, 50);
        //Lança o som
        let soundExplosion = this.sound.add('soundExplosion');
        soundExplosion.setVolume(1);
        soundExplosion.play();
        //Destroi a bala do player e do inimigo
        bulletPlayer.destroy();
        bulletEnemy.destroy();
    }

    //Define a colisão entre a bala do inimigo e o player
    bulletEnemyHitPlayer(player,bulletEnemy) {
        //Verifica se o numero de vidas do player é maior do que 0
        if (this.lives > 0) {
            //reduz uma vida
            this.lives--;
            //destroi a bala do inimigo
            bulletEnemy.destroy();
            //define a quantidade de vidas
            this.livesText.setText(this.lives);

        }
        //Verifica se o número de vidas é menos ou igual que 0
        if (this.lives <= 0) {
            //Lança o som soundPlayerDead e para o soundBack
            let soundPlayerDead = this.sound.add('soundPlayerDead');
            soundPlayerDead.setVolume(1);
            soundPlayerDead.play();
            this.soundBack.stop();
            //destroi a bala do inimigo e o player
            player.destroy();
            bulletEnemy.destroy();
            //Pausa as propriedades fisicas 
            this.physics.pause();
            //Define a cor do player como vermelho
            this.player.setTint(0xff0000);
            //define o estado do player isAlive como false
            this.isAlive = false;
            //Define que o gameOver como true
            this.gameOver = true;
            //Mostrar texto de game over 
            this.gameOverText.visible = true;
            //Score final
            this.finalScoreText = this.add.text(280, 400, 'Score: ' + this.score, {
                fontSize: '44px',
                fill: '#FFD700'
            });
            this.finalScoreText.setOrigin(0.5);
            this.finalScoreText.visible = true;

            //Define o pointerdown para restart
            this.input.on('pointerdown', () => {
                this.scene.start('preload');
                soundBack.play();
                soundBack.setLoop(true);
            });
        }
    }

    //Define a colisão entre a bala do player e o asteroid
    bulletPlayerHitAsteroids(bulletsPlayer, asteroids) {
        //Cria a particulas quando existe a colisao
        this.particles.emitParticleAt(asteroids.x, asteroids.y, 50);
        //Lança o som
        let soundExplosion = this.sound.add('soundExplosion');
        soundExplosion.setVolume(1);
        soundExplosion.play();
        //destroy a bala do player e o asteroide
        bulletsPlayer.destroy();
        asteroids.destroy();
    }

    //Define a colisão da bala do inimifo e o asteroid
    bulletEnemyHitAsteroids(bulletsEnemy, asteroids) {
        //Cria a particulas quando existe a colisao
        this.particles.emitParticleAt(asteroids.x, asteroids.y, 50);
        //Lança o som
        let soundExplosion = this.sound.add('soundExplosion');
        soundExplosion.setVolume(1);
        soundExplosion.play();
        //destroy a bala do inimigo e o asteroide
        bulletsEnemy.destroy();
        asteroids.destroy();
    }

    //Define a colisao entre o asteroid e o player
    asteroidHitPlayer(player, asteroid) {
        //Verifica se o numero de vidas do player é maior do que 0
        if (this.lives > 0) {
            //reduz uma vida
            this.lives--;
            //Destroi o asteroide
            asteroid.destroy();
            //Define o núemro de vidas
            this.livesText.setText(this.lives);
        }
        //Verifica se o número de vidas é menor ou igual a 0
        if (this.lives <= 0) {
            //Lança o som soundPlayerDead e para o soundBack
            let soundPlayerDead = this.sound.add('soundPlayerDead');
            soundPlayerDead.setVolume(1);
            soundPlayerDead.play();
            this.soundBack.stop();
            //Destroi o player e o asteroide
            player.destroy();
            asteroids.destroy();
            //Pausa as propriedades fisicas 
            this.physics.pause();
            //Define a cor do player como vermelho
            this.player.setTint(0xff0000);
            this.isAlive = false;
            //Define que o gameOver como true
            this.gameOver = true;
            //Mostrar texto de game over 
            this.gameOverText.visible = true;
            //Score final
            this.finalScoreText = this.add.text(280, 400, 'Score: ' + this.score, {
                fontSize: '44px',
                fill: '#FFD700'
            });
            this.finalScoreText.setOrigin(0.5);
            this.finalScoreText.visible = true;

            //Define o pointerdown para restart
            this.input.on('pointerdown', () => {
                this.scene.start('preload');
                soundBack.play();
                soundBack.setLoop(true);
            });
        }
    }

    //Define a colisão entre o player e o inimigo
    enemyHitPlayer(player,enemy) {
        //Verifica se o numero de vidas do player é maior do que 0
        if (this.lives > 0) {
            //rediz a vida do player
            this.lives--;
            //Destroi o inimigo
            enemy.destroy();
            //define as vidas do player
            this.livesText.setText(this.lives);
        }
        //Verifica se o número de vidas é menor ou igual a 0
        if (this.lives <= 0) {
            //Define o isAlive como falsde
            this.isAlive = false;
            //Lança o som soundPlayerDead e para o soundBack
            let soundPlayerDead = this.sound.add('soundPlayerDead');
            soundPlayerDead.setVolume(1);
            soundPlayerDead.play();
            this.soundBack.stop();
            //destroi o player e o grupo de inimigos
            player.destroy();
            enemy.destroy();

            //Pausa as propriedades fisicas 
            this.physics.pause();
            //Define a cor do player como vermelho
            this.player.setTint(0xff0000);
            //Define que o gameOver como true
            this.gameOver = true;
            //Mostrar texto de game over 
            this.gameOverText.visible = true;
            //Score final
            this.finalScoreText = this.add.text(280, 400, 'Score: ' + this.score, {
                fontSize: '44px',
                fill: '#FFD700'
            });
            this.finalScoreText.setOrigin(0.5);
            this.finalScoreText.visible = true;

            //Define o pointerdown para restart
            this.input.on('pointerdown', () => {
                this.scene.start('preload');
                soundBack.play();
                soundBack.setLoop(true);
            });
        }
    }

    //Ativa o powerUp
    activatePowerUp(player, powerUp){
        //destroi o powerUP
        powerUp.destroy();
        //Define o timer do powerUp a 0
        this.powerUpTime = 0;
        //define o estado do power up como true
        this.powerUpStatus = true;
    }

    //Função para update das scenes
    update(time) {
        this.updatePlayer(time);
    }

    //Função para dar update ao Player
    updatePlayer(time) {
        if (this.isAlive) {
            //Caso carregue na seta da esquerda
            if (this.cursors.left.isDown) {
                //define que o player sem move no sentido de x negativo
                this.player.setVelocityX(-200);
                this.player.setVelocityY(0);
            }
            //Caso carregue na seta da direita
            else if (this.cursors.right.isDown) {
                //define que o player sem move no sentido de x positivo
                this.player.setVelocityX(200);
                this.player.setVelocityY(0);
            } else if (this.cursors.down.isDown) {
                //define que o player sem move no sentido de x positivo
                this.player.setVelocityX(0);
                this.player.setVelocityY(200);
            } else if (this.cursors.up.isDown) {
                //define que o player não se move
                this.player.setVelocityX(0);
                this.player.setVelocityY(-200);
            } else {
                //define que o player não se move
                this.player.setVelocityX(0);
                this.player.setVelocityY(0);
            }
            //Verifica se o espaço está para baixo e se o time é maior que o lastBulletFired
            if (this.keySpace.isDown && time > lastBulletFired) {
                //Ativa a bullet do player
                this.addbullet(this.player.body.x, this.player.body.y, this.player.body.velocity.y);
                //soma o time mais 250 para definir o intervalo entre balas
                lastBulletFired = time + 250;
            }
            //verifica o estado do power up
            if (this.powerUpStatus) {
                //verifica se a tecla do ataque especial está para baixo e se o time é maior que o lastBulletSpecial
                if (this.keySpecial.isDown && time > lastBulletSpecial) {
                        //Ativa a bala especial
                        this.addSpecialBullet(this.player.body.x, this.player.body.y, this.player.body.velocity.x, this.player.body.velocity.y);
                        //soma o time mais 200 para definir o intervalo entre balas
                        lastBulletSpecial = time + 200;
                        //incrementsa o powerUpTime
                        this.powerUpTime += 1;
                } 
                //verifica se o powerUpTime == 40
                else if (this.powerUpTime == 40) {
                    //define o powerUpStatus como false
                    this.powerUpStatus = false;
                }
            }
        }
    }
}

export default GameScene